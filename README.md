# Jeremy - README

I created this personal README document to help you get to know me and how I work. This document is inspired by [Rayana Verissimo's README](https://gitlab.com/rverissimo/readme/blob/master/README.md) which she based on [Eric Johnson's](https://about.gitlab.com/handbook/engineering/erics-readme/) and [Jason Lenny's](https://gitlab.com/jlenny/focus/) READMEs.

* [GitLab Handle](https://gitlab.com/jeldergl)
* [Team page](https://about.gitlab.com/company/team/#jeremyelder)

## About Me

My name is Jeremy Elder and I’m a Staff Product Designer on the FE/UX Foundations team with a focus on the visual design of GitLab.

* I currently live in central Minnesota, USA, with my wife and our three youngsters.
* My normal working hours are around 7am – 4pm in the [CT Timezone](https://time.is/Minneapolis).
* I’ve been designing professionally for 20+ years and work on everything from UI design to logos and packaging. I also do some development.
* I care a great deal about accessibility and tailor my work as such.
* Mountain biking, camping, and boating are some of the things I enjoy most when not working.

## Working values

* Distill concepts into responsible and honest work.
* Digital goods can be durable goods too.
* Constraints create flow and movement.
* Think in systems, design in moments.
* Follow your users, not trends.
* Clarity over embellishement.
* Iteration is perfection.
* Learn with grace.

## My role at GitLab

I joined GitLab in May of 2019 to focus on the visual design of the product. I care about the relationship between product functionality and the consistency of the UI. I focus on creating seamless, performant, and accessible experiences for users as they engage in everything GitLab has to offer. I believe that [a great UI is invisible](https://tympanus.net/codrops/2013/03/21/a-great-ui-is-invisible/) — transparency is one of our values after all :wink:.

## Connect

* [Contact me](mailto:jelder@gitlab.com)
* [Dribbble](https://dribbble.com/jeremyelder)
* [Twitter](https://twitter.com/jeremyelder)
* [jeremyelder.com](https://jeremyelder.com)
